<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@home');
Route::get('/user/login', 'UserController@login');
Route::get('/users', 'UserController@index');
Route::get('/users/{id}', 'UserController@show');
Route::get('/user/handlefacebookcallback', 'UserController@handleFacebookCallback');
Route::get('/user/onboarding', 'UserController@onboarding');
Route::post('/user/onboarding', 'UserController@handleOnboarding');
Route::post('/compliment/{id}', 'ComplimentController@store');
Route::get('/compliments/received', 'ComplimentController@received');