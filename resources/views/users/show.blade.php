@extends('layouts.app')

@section('content')
<div class="container">
	
	<div class="row">
		{{ $user->name }}
	</div>
	
	<form action="/compliment/{{ $user->id }}" method="post">
		<label for="compliment">Compliment</label>
		<input type="text" name="compliment" id="compliment">
		<input type="submit" value="Give compliment">
		{{csrf_field()}}
	</form>

	<h2>Compliments</h2>

	@foreach($compliments as $c)
		<article>{{ $c->text }}</article>
	@endforeach

</div>
@endsection