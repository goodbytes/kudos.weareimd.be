@extends('layouts.app')

@section('content')
    <div>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

    </div>


	<div class="row">
		<div class="col users">
			@foreach( $users as $u )
			<div class="user">
				<a class="user__link" href="/users/{{ $u->id }}">
					<img class="user__avatar" src="{{ $u->avatar }}" alt="" >
				</a>
			</div>
			@endforeach
		</div>
	</div>
@endsection