@extends('layouts.app')

@section('content')
    <form action="" method="post">
		<div class="row onboarding">
    			<h2>Almost there</h2>
                <label for="bio ">Enter a short bio below. What should people know about you?</label>
                <input name="bio" class="form-control" type="text" value="" id="bio">
                <p class="help-text">I like coding and pounding &#9889;techno music&#9889;</p>
            
                {{ csrf_field() }}
		</div>
    </form>
@endsection