@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<div class="alert alert-dark" role="alert">
				Welcome <strong>{{ $user->name }}</strong>
			</div>
		</div>
	</div>
@endsection