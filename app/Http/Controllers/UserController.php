<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;

class UserController extends Controller
{
	public function index(){
        $users = \App\User::all();
    	return view('users/index', compact('users'));
	}

	public function onboarding(){
		return view('users/onboarding');
	}

	public function handleOnboarding(){
       $user = \Auth::user();
       $bio = request()->get('bio');
       $user->bio = $bio;
       $user->save();
       dd($bio);
	}

  public function show($id){
    $u = \App\User::find($id);
    $compliments = $u->compliments;

    return view('users/show')->with('user', $u)->with('compliments', $compliments);
  }

    public function login(){
    	return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(){
    	// first we find the facebook user
    	$facebookUser = Socialite::driver('facebook')->user();
    	
    	// next, we'll try to find the user in our database
    	// if it doesn't exist yet, it will be created
    	$user = \App\User::firstOrNew(['email' => $facebookUser->email]);
    	$user->name = $facebookUser->name;
    	$user->avatar = $facebookUser->avatar;
    	$user->avatar_original = $facebookUser->avatar_original;
    	$user->gender = $facebookUser->user['gender'];
    	$user->token = $facebookUser->token;
		$user->Save();

        \Auth::login($user);
		
		// check if a user should be onboarded first 
		if( !isset( $user->bio ) || empty( $user->bio ) ){
		//	return redirect('/user/onboarding');
		}

		// redirect to the /home route
        return redirect('/home');

	}
	

}

