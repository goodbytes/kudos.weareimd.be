<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ComplimentController extends Controller
{
    public function store($id){
    	$c = new \App\Compliment();
    	$c->user_id = $id;
    	$c->text = request()->get('compliment');
    	$c->Save();

    	return redirect('/users/'.$id);
    }

    public function received(){

    		$compliments = \App\Compliment::where('user_id', "=", \Auth::user()->id)->get();
    		return view('compliments/received')->with('compliments', $compliments);

    }
}
