<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
    	$user = \Auth::user();
    	$data['user'] = $user;
    	return view('home/index', $data);
    }
}
